import axios, { CancelToken } from 'axios';

/**
 * Abstraction wrapper around the module being used to handle AJAX requests
 */
export default class Ajax {

    static CONST__DEFAULTS = {
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'X-Requested-With': 'XMLHttpRequest'
        }
    };

    constructor(options) {
        this.client = axios.create(Object.assign(Ajax.CONST__DEFAULTS, options));
    }

    /**
     * Perform a POST request
     *
     * @param {string} url        URL to send the POST request to
     * @param {object} data       Data to be POSTed to the server with the request
     * @param {object} additional [Optional] Additional params, data, headers to be attached to request
     *
     * @returns {Promise}
     */
    post(url, data, additional = {}) {
        return this.client
            .request({
                method: 'post',
                url,
                data,
                ...additional
            })
            .catch(Ajax.catchError);
    }

    /**
     * Perform a GET request
     *
     * @param {string} url        The URL to send the request to
     * @param {object} params     [Optional] Query parameters to attach to the request
     * @param {object} additional [Optional] Additional params, data, headers to be attached to request
     *
     * @returns {Promise}
     */
    get(url, params = {}, additional = {}) {
        return this.client
            .request({
                method: 'get',
                url,
                params,
                ...additional
            })
            .catch(Ajax.catchError);
    }

    /**
     * Perform a PUT request
     *
     * @param {string} url        The URL to send the request to
     * @param {object} params     [Optional] Query parameters to attach to the request
     * @param {object} additional [Optional] Additional params, data, headers to be attached to request
     *
     * @returns {Promise}
     */
    put(url, params = {}, additional = {}) {
        return this.client
            .request({
                method: 'put',
                url,
                params,
                ...additional
            })
            .catch(Ajax.catchError);
    }

    /**
     * Perform a DELETE request
     *
     * @param {string} url        The URL to send the request to
     * @param {object} params     [Optional] Query parameters to attach to the request
     * @param {object} additional [Optional] Additional params, data, headers to be attached to request
     *
     * @returns {Promise}
     */
    delete(url, params = {}, additional = {}) {
        return this.client
            .request({
                method: 'delete',
                url,
                params,
                ...additional
            })
            .catch(Ajax.catchError);
    }

    /**
     * Cancelable Request Creator
     * This function will allow for requests that have been made to be cancelled and replaced with a new request
     *
     * @param {string} method the axios method to use
     *
     * @returns {Function}
     */
    cancelable(method = 'get') {
        let cancelSource;
        return (url, data) => {
            if (cancelSource) {
                cancelSource.cancel('Cancelled request');
            }
            cancelSource = CancelToken.source();

            return this[method](url, data, { cancelToken: cancelSource.token });
        };
    }

    postFormData(url, data, additional = {}) {
        const additionalData = {

            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'X-Requested-With': 'XMLHttpRequest'
            },
            ...additional
        };
        return this.client
            .request({
                method: 'post',
                url,
                data,
                ...additionalData
            })
            .catch(Ajax.catchError);
    }

    static catchError(error) {
        if (axios.isCancel(error)) {
            console.info(error.message);
        } else if (error.response && error.response.status === 401) {
            error.response.data = {
                message: 'Your session has expired. Please refresh your page to continue'
            };
        }
        throw error;
    }

}
