/**
 * localStorage abstraction
 */

export default class Storage {

    /**
     * Get item from local storage. Will parse JSON before resolving Promise
     *
     * @param {string} key The key of the item to be loaded from storage
     *
     * @returns {Promise}
     */
    static getItem(key) {
        return new Promise((resolve) => {
            resolve(localStorage.getItem(key));
        }).then((res) => {
            return JSON.parse(res);
        });
    }

    /**
     * Save data to storage
     *
     * @param {string}        key   The key of the item to be saved
     * @param {string|object} value The value being saved
     *
     * @returns {Promise}
     */
    static setItem(key, value) {
        return new Promise((resolve, reject) => {
            try {
                localStorage.setItem(key, JSON.stringify(value));
                resolve(value);
            } catch {
                const error = new Error(`Failed to set ${key}`);
                reject(error);
            }
        });
    }

    /**
     * Remove an item from storage
     *
     * @param {string} key The key of the item to be removed
     *
     * @returns {Promise}
     */
    static removeItem(key) {
        return new Promise((resolve, reject) => {
            try {
                localStorage.removeItem(key);
                resolve(true);
            } catch {
                const error = new Error(`Failed to remove ${key}`);
                reject(error);
            }
        });
    }

}
