/* eslint-disable filenames/match-regex */
import Ajax from './Ajax';

export class TranslatorController {

    constructor() {
        this.dictionary = {};
        this.ajax = new Ajax();
    }

    async init(translationFileUrl) {
        return this.ajax
            .get(translationFileUrl)
            .then(({ data }) => {
                this.dictionary = data;
            });
    }

    translate(term) {
        if (term in this.dictionary) {
            return this.dictionary[term];
        }
        return term;
    }

}

export const Translator = new TranslatorController();

// eslint-disable-next-line no-underscore-dangle
export const __ = (term) => Translator.translate(term);
