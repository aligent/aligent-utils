/**
 * Converts a query string to a json object
 *
 * eg. Converts form_key=FkERZA1iRAuoyFs6&product=12 to
 * {
 *     form_key: "FkERZA1iRAuoyFs6",
 *     product: "12"
 * }
 *
 * @param {string}      queryString  The query string to convert
 * @returns {object}    json object containing data from the queryString
 */
export default function (queryString) {
    return JSON.parse(`{ "${decodeURI(queryString).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"')}" }`);
}
