const DEFAULT_OFFSET = 50;

/**
 * @param {number} scrollTo position of y axis
 * @param {number} duration duration in milliseconds
 */
function scrollToTop(scrollTo, duration = 200) {
    const scrollStep = -window.scrollY / (duration / 15);
    const scrollInterval = setInterval(() => {
        if (window.scrollY >= scrollTo) {
            window.scrollBy(0, scrollStep);
        } else clearInterval(scrollInterval);
    }, 15);
}

/**
 * @param {Element} element element to getTop offset
 */
export default function scrollToElementTop(element) {
    scrollToTop(element.offsetTop - DEFAULT_OFFSET);
}
