/* global jQuery */

export default (storageData) => {
    return new Promise((resolve) => {
        // Invalidate the cart's local storage so that it will refresh on the next non-checkout page load
        const currentStorageInvalidation = JSON.parse(
            localStorage.getItem('mage-cache-storage-section-invalidation')
        );
        const nextStorageInvalidation = { ...currentStorageInvalidation, cart: true };
        localStorage.setItem(
            'mage-cache-storage-section-invalidation',
            JSON.stringify(nextStorageInvalidation)
        );
        // update mage storage
        const currentStorage = JSON.parse(localStorage.getItem('mage-cache-storage'));
        const nextStorage = { ...currentStorage, ...storageData };
        localStorage.setItem('mage-cache-storage', JSON.stringify(nextStorage));

        // triggers customerData to update all the things. - messages, cart, wistlist, etc
        try {
            jQuery(document).trigger('customer:update');
        } catch (e) {
            console.log(e);
        }
        resolve();
    });
};
