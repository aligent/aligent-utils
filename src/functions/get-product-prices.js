import get from 'lodash/get';
import formatPrice from './format-price';

/**
 * Get the regular and sale price, if any, of the provided item
 *
 * @param {object} item The item to get the prices for
 * @param {string} priceFormat the prices' format
 * @returns {object}
 */
export default (item, priceFormat) => {
    const product = get(item, 'product', item);
    const regularPrice =        product.price > item.base_price_incl_tax
        ? product.price
        : item.base_price_incl_tax || item.price;
    const onSale = product.special_price || product.price !== item.base_price_incl_tax;
    const salePrice = item.base_price_incl_tax;

    return {
        regularPrice: formatPrice(regularPrice, priceFormat, true),
        salePrice: formatPrice(salePrice, priceFormat, true),
        onSale
    };
};
