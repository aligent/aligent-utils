export default (countryCode, allRegions = []) => {
    if (countryCode === 'NZ') {
        return []; // New Zealand doesn't have states
    }
    return allRegions.filter((region) => region.country_id === countryCode || region.value === '');
};
