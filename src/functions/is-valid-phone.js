import validateString from './validate-string';

const TELEPHONE_PATTERN = /^[-+\s()0-9]{6,24}$/;

/**
 * Validate that a provided value looks like a phone number
 * The only validation required is that the number is at least 1 number long, and only number
 * are included
 *
 * @param {string} number The number to validate
 *
 * @returns {boolean}
 */
export default (number) => {
    return validateString(number, TELEPHONE_PATTERN);
};
