/**
 * Converts object to form data string.
 *
 * @param {object} data the object to convert
 * @param {string} [prefix=null] a prefix for form key
 *
 * @returns {string}
 */
export default function objectToFormData(data, prefix = null) {
    return Object.keys(data)
        .map((key) => {
            const value = data[key];
            const keyString = prefix ? `${prefix}[${key}]` : key;
            if (typeof value !== 'object') {
                return `${keyString}=${value}`;
            }
            return objectToFormData(value, keyString);
        })
        .join('&');
}
