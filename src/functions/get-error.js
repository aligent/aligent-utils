export default (error, selector) => {
    try {
        if (error.response && error.response.data && typeof error.response.data.message === 'string') { return error.response.data.message; }
        if (typeof error.message === 'string') { return error.message; }
        if (typeof error === 'string') { return error; }
        if (typeof error[selector] === 'string') { return error[selector]; }
    } catch {
        return 'Something when wrong.';
    }
    return 'Something when wrong.';
};
