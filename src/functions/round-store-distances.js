export default (stores) => stores.map((store) => {
    const roundedDistance = Number(store.distance).toFixed(1);
    return {
        ...store,
        distance: `${roundedDistance} km`
    };
});
