import validateString from './validate-string';

/**
 * Test a given value to see if it is a discount code
 * Discount codes will have the form "Discount (code)", where 'code' is whatever the applied coupon code is.
 * We can't use a simple check of the `code` property in the totals_segment because catalog price rules, which don't
 * have a coupon code, and can't be removed, also have the `code` of "discount".
 *
 * @param {string} value value of discount
 * @returns {*}
 */
export default (value) => {
    return validateString(value, /Discount \(.*\)/);
};
