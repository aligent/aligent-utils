
/**
 * Generates simple low entropy GUID
 *
 * @param {number} length of GUID
 * @param {Array} separatorIndex where to add the separators
 *
 * @returns {string} the GUID
 */
export default function generateSimpleGuid(length = 32, separatorIndex = [8, 12, 16, 20]) {
    return Array.from(Array(length).keys()).reduce((result, number, index) => {
        if (separatorIndex.includes(index)) {
            return `${result}-`;
        }
        const randomID = Math.floor(Math.random() * 16)
            .toString(16)
            .toUpperCase();
        return `${result}${randomID}`;
    }, '');
}
