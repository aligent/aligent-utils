/**
 * Process a Magento error object, and return a string representation
 *
 *
 * @param {object}  error             Contains information regarding the error
 * @param {string}  error.message    The error message, with possible placeholder values
 * @param {Array}   error.parameters Array of values that would replace placeholder values in above message
 *
 * @returns {string}
 */
export default (error) => {
    const { message, parameters = [] } = error;
    if (parameters.length === 0) return message;

    return Object.keys(parameters).reduce((msg, param) => {
        const pattern = message.search('"%') >= 0 ? `"%${param}"` : `%${parseInt(param, 10) + 1}`;
        const fieldName = parameters[param];
        const regex = new RegExp(pattern, 'g');
        return msg.replace(regex, `'${fieldName}'`);
    }, message);
};
