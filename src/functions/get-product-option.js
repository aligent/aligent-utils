/**
 * Get the selected configurable option for a product in the cart
 *
 * @param {object} product The product to get the configurable option for
 * @param {string} optionLabel the option to load
 *
 * @returns {object}
 * @example {
 *     option_id: 202,
 *     option_label: 'size',
 *     option_value: '13',
 *     product_id: '263677',
 *     selected: false,
 *     sku: 'CF10088BLACA350',
 *     value: 'XEUW350'
 * };
 */

export default (product, optionLabel) => {
    if (!product.configurable_attributes) {
        return false;
    }
    const selectedAttribute = product.configurable_attributes.find(
        (attribute) => attribute.attribute_code === optionLabel
    );
    if (!selectedAttribute) return false;
    const selectedOption = selectedAttribute.options.find((option) => option.selected);
    // attr.option_id is an integer, whereas option is a string. Casting both to int just to be safe
    return selectedOption;
};
