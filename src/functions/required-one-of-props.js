/**
 * Creates a propCheck function based on the props
 *
 * @param {Array} properties Props on component where at least one is required.
 *
 * @returns {Function}
 */
export default (properties = []) => {
    /**
     * See https://reactjs.org/docs/typechecking-with-proptypes.html#react.proptypes
     * A custom PropTypes validator
     *
     * @param {object} props All props from component
     * @param {string} propName Current property name
     * @param {string} componentName name of component
     *
     * @returns {Error|void} if fails validation
     */
    // eslint-disable-next-line consistent-return
    return (props, propName, componentName) => {
        if (properties.every((prop) => !props[prop])) {
            return new Error(
                `One of ${properties.join(', ')} is required by '${componentName}' component.`
            );
        }
    };
};
