import validateString from './validate-string';

/**
 * Function that will validate an email address to ensure it, at the very least, includes an @ symbol and a dot
 *
 * @param {string} email The email to test
 *
 * @returns {boolean}
 */
export default (email) => {
    return validateString(email, /(.+)@(.+)\.(.+){2,}/);
};
