export default (item, data) => parseInt(item.id, 10) === parseInt(data.id, 10);
