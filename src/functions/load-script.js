/**
 * Dynamically loads script url, appends to DOM, and resolve a promise when loaded.
 *
 * @param {string} url  URL of script to load
 *
 * @returns {Promise}   Promise will either resolve or reject based on whether the script can load.
 */
export default (url) =>
    new Promise((resolve, reject) => {
        if (!url) {
            reject(new Error('No script provided'));
        }
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        script.onload = resolve;
        script.onerror = reject;
        document.body.appendChild(script);
    });
