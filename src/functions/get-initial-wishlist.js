export default () => {
    try {
        const { wishlist } = JSON.parse(localStorage.getItem('mage-cache-storage'));
        return wishlist;
    } catch (error) {
        console.log('No wishlist state in mage-cache-storage');
        return {};
    }
};
