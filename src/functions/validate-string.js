/**
 * Checks the if value is a valid code
 *
 * @param {string} value string you wish to test
 * @param {RegExp} pattern pattern of test
 *
 @returns {boolean}
 */
export default (value, pattern) => new RegExp(pattern, 'gim').test(value);
