/**
 * Pluralise a string if its quantity is 0 or greater than 1.
 * If the plural form of the word's more complicated than adding
 * an 's' on the end, accepts a third parameter for the plural form.
 *
 * @param {string} singular The singular string to pluralise
 * @param {number} amount Amount to check whether to pluralise
 * @param {string} plural   Plural form if more complicated than appending an 's'
 *
 * @returns {string}
 */
export default (singular, amount, plural) => {
    const isPlural = amount !== 1;

    if (isPlural) {
        // if plural form has been given, use that;
        // otherwise append an 's' to the singular form
        return typeof plural !== 'undefined' ? plural : `${singular}s`;
    }

    return singular;
};
