/**
 * Get other available configurable options for a given products configurable
 *
 * @param {object} product The product to get all configurables for
 * @param {string} option  The configurable to load
 *
 * @returns {Array|boolean}
 */
export default (product, option) => {
    if (!product.configurable_attributes) {
        return false;
    }

    const configurable = product.configurable_attributes.find((config) => config.attribute_code === option);

    if (!configurable) {
        return false;
    }

    return configurable.options.map((opt) => {
        return {
            value: opt.value,
            label: opt.value,
            disabled: !opt.is_in_stock
        };
    });
};
