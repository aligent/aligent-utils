/**
 * formats street into array with a max length of 36 chars per street line
 *
 * @param {string|Array} street the street value - if from saved address could be array
 *
 * @returns {Array} magento wants it formatted into an array of stringLengths no more than 36 chars.
 */
export default (street) => {
    if (Array.isArray(street)) {
        return street;
    }
    if (street.length > 36) {
        // NOTE should handle on the street field a maxLength of 72 char so the street[1] isnt more than 36
        // Not capping here as magento should return error if too long.
        return [street.slice(0, 36), street.slice(36)];
    }
    return [street];
};
