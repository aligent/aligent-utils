/**
 * Safely convert HTML taged text to regular text.
 *
 * @param {string} html Text you wish to clean.
 *
 * @returns {string}
 */
export default function stripHtml(html) {
    const div = document.createElement('div');
    div.innerHTML = html;
    return div.textContent || div.innerText || '';
}
