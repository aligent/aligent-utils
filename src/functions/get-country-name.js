export default (id, countries) => {
    return countries.find((country) => country.value === id).label;
};
