import validateString from './validate-string';

/**
 * For checking if a string is blank, null or undefined including '       '
 *
 * @param {string} string The email to test
 *
 * @returns {boolean}
 */
export default (string) => !string || validateString(string, /^\s*$/);
