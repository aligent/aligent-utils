/**
 * Indicates if the checkout is using the mobile UI
 *
 * This can be used to customize some of the "automatic" actions that might take place on desktop, that shouldn't
 * take place on mobile, or vice versa
 *
 * @returns {boolean}
 */
export default function isMobileScreen() {
    // The width below at which the UI will change to tablet/mobile
    return window.innerWidth < isMobileScreen.CONST__MOBILE_BREAKPOINT;
}

isMobileScreen.CONST__MOBILE_BREAKPOINT = 960;
