import getRegions from './get-regions';

export default (countryCode, allRegions, regionCode) => {
    if (regionCode === '') {
        return '';
    }
    const localRegions = getRegions(countryCode, allRegions);
    const activeRegion = localRegions.find((region) => region.value === regionCode);
    if (!activeRegion) {
        return '';
    }
    return activeRegion.title;
};
