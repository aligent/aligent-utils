/* eslint-disable no-extra-parens */

/**
 * Returns an elements data attributes as an object,
 * whilst attempting to format the attribute values
 * as JSON.
 *
 * @param  {any} element [description]
 * @returns {any}         [description]
 */
export default function (element) {
    return (
        element
        && element.dataset
        && Object.keys(element.dataset).reduce((result, key) => {
            const data = element.dataset[key];
            try {
                result[key] = (data && JSON.parse(data)) || {};
            } catch (err) {
                result[key] = data;
            }
            return result;
        }, {})
    );
}
