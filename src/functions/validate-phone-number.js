export default (number) => /^[-+\s()0-9]{6,24}$/.test(number);
