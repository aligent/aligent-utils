// Return the default address of type (delivery or billing) or the first address if there isn't one
export default (addresses, type) => addresses.find((address) => address[`default_${type}`]) || addresses[0];
