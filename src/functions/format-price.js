/**
 * Format a provided price according to the pattern provided
 *
 * @param {string}     price        The number of the price
 * @param {string}  pattern      The pattern for prices
 * @param {boolean} showDecimals Flag to indicate if the decimals should be shown regardless of value
 * @param {boolean} showFree     Flag to indicate if the value is $0 then should return Free
 *
 * @returns {string}
 */
export default (price = '', pattern = '$%s', showDecimals = false, showFree = true) => {
    let parsedPrice = parseFloat(`${price}`.replace('$', '')) || 0;
    let prefix = '';

    if (parsedPrice === 0 && showFree) {
        return 'Free';
    }

    if (parsedPrice < 0) {
        parsedPrice *= -1;
        prefix = '-';
    }

    if (showDecimals || Math.round(parsedPrice) !== parsedPrice) {
        return `${prefix}${pattern.replace('%s', parsedPrice.toFixed(2))}`;
    }

    return `${prefix}${pattern.replace('%s', parsedPrice)}`;
};
