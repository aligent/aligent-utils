/* eslint-disable max-len */

/**
 * Taken from vendor/magento/module-ui/view/base/web/js/lib/validation/rules.js
 *
 * @param {string} value the value of the password
 * @param {number} [passwordMinLength=8] minimum password length
 * @param {number} [passwordMinCharacterSets=3] minimum password character types
 * @param {boolean} force force validation if empty value
 *
 * @returns {object}
 */
export default function validateCustomerPassword(
    value,
    passwordMinLength = 8,
    passwordMinCharacterSets = 3,
    force = false
) {
    if (!force && value === '') {
        return {
            valid: true,
            message: ''
        };
    }

    const mainMessage = 'Password must be a minimum of %1 characters and should include a mix of lower and upper case letters, numbers and special characters.'.replace(
        '%1',
        passwordMinLength
    );

    let counter = 0;
    // const passwordMinLengthMessage = 'Minimum of different classes of characters in password is %1. Classes of characters: Lower Case, Upper Case, Digits, Special Characters.'.replace(
    //     '%1',
    //     passwordMinLength
    // );

    // const passwordMinCharacterSetsMessage = 'Minimum length of this field must be equal or greater than %1 symbols. Leading and trailing spaces will be ignored.'.replace(
    //     '%1',
    //     passwordMinCharacterSets
    // );

    if (value.length < passwordMinLength) {
        return {
            valid: false,
            message: mainMessage
        };
    }

    if (value.match(/\d+/)) {
        counter += 1;
    }

    if (value.match(/[a-z]+/)) {
        counter += 1;
    }

    if (value.match(/[A-Z]+/)) {
        counter += 1;
    }

    if (value.match(/[^a-zA-Z0-9]+/)) {
        counter += 1;
    }

    if (counter < passwordMinCharacterSets) {
        return {
            valid: false,
            message: mainMessage
        };
    }

    return {
        valid: true,
        message: ''
    };
}
