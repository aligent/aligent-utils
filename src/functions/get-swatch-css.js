/* eslint-disable no-mixed-operators */

/**
 * Get background css for swatch element
 *
 * @param {string} swatch - array|hex|url
 * @returns {{background: string}}
 */
export default (swatch) => {
    if (!swatch) return { background: '' };
    const HEX_COLOUR_REGEX = /^#([0-9a-f]{3}|[0-9a-f]{6})$/i;
    const isMultiColor = swatch.split(',').length > 1;
    const isHex = swatch.match(HEX_COLOUR_REGEX);

    const getMultiColor = (colors) => {
        const incr = 100 / colors.length;
        let step = 0;

        const formatted = colors.map((color) => {
            const from = `${color} ${step}%`;
            step += incr;
            const to = `${color} ${step}%`;
            return [from, to].join(', ');
        }).join(', ');

        return `linear-gradient(to right, ${formatted})`;
    };

    const getImage = (url) => {
        const baseUrl = '/media/attribute/swatch';
        return `url(${baseUrl}${url}) no-repeat center`;
    };

    if (isMultiColor) {
        return { background: getMultiColor(swatch.split(',')) };
    }

    if (isHex) {
        return { background: swatch };
    }

    return { background: getImage(swatch) };
};
