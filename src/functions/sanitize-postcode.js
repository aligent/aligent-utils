// Remove letters from a string and shorten it to form a valid Australian or New Zealand postcode
export default (input) => {
    return input.replace(/[^0-9]/, '').slice(0, 4);
};
