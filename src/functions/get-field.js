import get from 'lodash/get';
import has from 'lodash/has';

export default (data, key = 'value') => {
    if (key !== 'value' && has(data, key)) {
        return get(data, key) || '';
    }
    return has(data, 'value') ? get(data, 'value') : data || '';
};
