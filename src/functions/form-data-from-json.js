import objectToFormData from './object-to-form-data';

export default (data) => encodeURI(objectToFormData(data));
