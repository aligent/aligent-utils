import validateString from './validate-string';

/**
 *Checks the if value is a valid code
 *
 * @param {string} value string you wish to test
 * @param {string} [pattern=''] pattern of test defaults to true
 *
 @returns {boolean}
 */

export default (postcode, pattern = '[0-9]{4}') => validateString(postcode, pattern);
