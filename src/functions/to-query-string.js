/**
 * Converts objects to query strings joining keys and values by &
 *
 * @param {object} params
 * @param {string} prefix
 *
 * @returns {string} string query
 */

const toQueryString = (params = {}, prefix = null) => {
    const query = Object.keys(params).map((k) => {
        let key = k;
        let value = params[key];

        if (!value && (value === null || value === undefined || Number.isNaN(value))) {
            value = '';
        }

        switch (params.constructor) {
            case Array:
                key = `${prefix}[]`;
                break;
            case Object:
                key = prefix ? `${prefix}[${key}]` : key;
                break;
            default:
                return key;
        }

        if (typeof value === 'object') {
            return toQueryString(value, key); // for nested objects
        }

        return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
    });

    return query.join('&');
};

export default toQueryString;
