import validateString from './validate-string';

const STREET_VALIDATION = /(([\d]+ [\w]*){1,}|([\w]* [\w]*){2,})/;

/**
 * checks street has at least a single number, a letter and space.
 *
 * @param {string} street the street value
 *
 * @returns {boolean}
 */
export default (street) => validateString(street, STREET_VALIDATION);
