const ADDRESS_FIELDS = [
    'firstname',
    'lastname',
    'company',
    'phone',
    'street',
    'suburb',
    'region',
    'postcode'
];

export default (address) => address.usingStored || ADDRESS_FIELDS.every((field) => !address[field] || address[field].valid);
