import getRegionNameByCode from './get-region-name-by-code';

/**
 * Take the billing address object from the redux state and return the
 * appropriate formatted billing address based on stored addresses and
 * manually entered addresses.
 *
 * @param {object} billingAddress the billing address object
 * @param {string} defaultCountryId the 2 char string char country representation
 * @param {string} regionId the region id
 *
 * @returns {object}
 */
export default (billingAddress, defaultCountryId, regionId) => {
    const { usingStored, storedAddresses, selectedStored } = billingAddress;

    if (!usingStored) {
        return {
            name: [billingAddress.firstname.value, billingAddress.lastname.value].join(' '),
            street: billingAddress.street.value,
            suburb: billingAddress.suburb.value,
            ...billingAddress.region && {
                region: getRegionNameByCode(defaultCountryId, regionId, billingAddress.region.value)
            },
            phone: billingAddress.phone.value
        };
    }

    const {
        firstname,
        lastname,
        city,
        region,
        street,
        telephone
    } = storedAddresses.find(({ id }) => id === selectedStored);

    return {
        name: [firstname, lastname].join(' '),
        suburb: city,
        region: region.name,
        phone: telephone,
        street
    };
};
