# Aligent JS Utility Functions

A general purpose library to be used for generic functions around manipulating data.

Dependencies on [Axios](https://github.com/axios/axios) and [Lodash](https://lodash.com/).


## Install

`npm install @aligent/utils`


### Ajax Class

A small wrapper around axios.

Use case

```js
import { Ajax } from '@aligent/utils';

const myAjaxRequest = new Ajax();

/**
 * Perform a POST request
 *
 * @param {string} url        URL to send the POST request to
 * @param {object} data       Data to be POSTed to the server with the request
 * @param {object} additional [Optional] Additional params, data, headers to be attached to request
 *
 * @returns {Promise}
 */
await myAjaxRequest.post(url, data, additional)
```

Can also use `.get()`, `.put()`, `.delete()` the same way.


### Translator Class

A way to fetch the translation map object

Use

```js
import { Translator, __ } from '@aligent/utils';

// fire ajax to file
Translator.init('/path-to-translation-file');

// translate string - will return same string if not found.
__('My Cart') //=> eg Bag
```

### Storage Class

localStorage abstraction wrapped in Promises.

Use:

```js
import { Storage } from '@aligent/utils';

await Storage.setItem('mykey', 'myvalue');
await Storage.getItem('mykey');
await Storage.removeItem('mykey');
```
