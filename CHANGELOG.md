# 1.1.3

- Fix issue with the Ajax catchError function

# 1.1.2

- Transpile the project

# 1.1.1

- Add queryStringToJson function

# 1.1.0

- Added linting for filenames and import names
- Updated tests

# Initial release 1.0.0

- Pulled and merged utils from various react projects
- Updated tests
