import { queryStringToJson } from '../src';

describe('queryStringToJson', () => {
    it('converts a simple query string', () => {
        const queryString = 'form_key=FkERZA1iRAuoyFs6&product=12';
        const expectedObject = {
            form_key: 'FkERZA1iRAuoyFs6',
            product: '12'
        };
        expect(queryStringToJson(queryString)).toEqual(expectedObject);
    });

    it('converts a query string with quotes', () => {
        const queryString = 'form_key=FkER"ZA\'1iRAuoyFs6&product=12';
        const expectedObject = {
            form_key: 'FkER"ZA\'1iRAuoyFs6',
            product: '12'
        };
        expect(queryStringToJson(queryString)).toEqual(expectedObject);
    });

    it('converts a query string with url encoded characters', () => {
        const queryString = 'abc=foo&def=%5Basf%5D&xyz=5';
        const expectedObject = {
            abc: 'foo',
            def: '[asf]',
            xyz: '5'
        };
        expect(queryStringToJson(queryString)).toEqual(expectedObject);
    });
});
