import axios from 'axios';
import { Ajax } from '../src';
import MockAdapter from 'axios-mock-adapter';

const mock = new MockAdapter(axios);


describe('Ajax', () => {

    const ajax = new Ajax();
    const data = { result: 1, success: true };
    mock.onPost('/test').reply(200, data);
    mock.onGet('/test').reply(200, data);
    mock.onPut('/test').reply(200, data);
    mock.onDelete('/test').reply(200, data);


    it('Gets', async () => {
        await ajax.get('/test').then((response) => { expect(response.data).toEqual(data) });
    });

    it('Posts', async () => {
        await ajax.post('/test').then((response) => { expect(response.data).toEqual(data) });
    });

    it('Puts', async () => {
        await ajax.put('/test').then((response) => { expect(response.data).toEqual(data) });
    });

    it('Deletes', async () => {
        await ajax.delete('/test').then((response) => {
            expect(response.data).toEqual(data);
        });
    });

    it('Handles errors without response object', () => {
        const catchErrorFunction = () => {
            Ajax.catchError(new Error('Generic Error'));
        }

        expect(catchErrorFunction).toThrow('Generic Error');
    });
});
