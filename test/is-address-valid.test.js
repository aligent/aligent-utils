import { isAddressValid } from '../src';

describe('isAddressValid', () => {
    it('Is always valid when using a stored address', () => {
        expect(isAddressValid({ usingStored: true })).toEqual(true);
    });

    it('Returns false if any of the fields is invalid', () => {
        const address  = {
            firstname: { valid: false },
            lastname: { valid: true },
            company: { valid: true },
            phone: { valid: true },
            street: { valid: true },
            suburb: { valid: true },
            region: { valid: true },
            postcode: { valid: true }
        };

        expect(isAddressValid(address)).toEqual(false);
    });

    it('Returns true if all of the fields are valid', () => {
        const address  = {
            firstname: { valid: true },
            lastname: { valid: true },
            company: { valid: true },
            phone: { valid: true },
            street: { valid: true },
            suburb: { valid: true },
            region: { valid: true },
            postcode: { valid: true }
        };

        expect(isAddressValid(address)).toEqual(true);
    });

    it('Ignores one of the fields if it doesn\'t exist', () => {
        const address = {
            firstname: { valid: true },
            lastname: { valid: true },
            company: { valid: true },
            phone: { valid: true },
            street: { valid: true },
            suburb: { valid: true },
            // Region is gone
            postcode: { valid: true }
        }

        expect(isAddressValid(address)).toEqual(true);
    });
});
