function findAction(store, type) {
    return store.getActions().find(action => action.type === type);
}

export default function getTestAction(store, type) {
    const action = findAction(store, type);
    if (action) return Promise.resolve(action);

    return new Promise((resolve) => {
        store.subscribe(() => {
            const actionThunk = findAction(store, type);
            if (actionThunk) resolve(actionThunk);
        });
    });
}
