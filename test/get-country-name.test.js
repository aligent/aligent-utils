import { getCountryName } from '../src';

describe('getCountryName', () => {
    const testCountries = [
        { value: 'AU', label: 'Australia' }
    ];

    it('Gets the country name', () => {
        expect(getCountryName('AU', testCountries)).toEqual('Australia');
    });
});
