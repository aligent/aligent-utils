import { roundStoreDistances } from '../src';

describe('roundStoreDistances', () => {
    it('Rounds distances', () => {
        expect(roundStoreDistances([{ distance: '1.111' }])).toEqual([{ distance: '1.1 km' }]);
    });
});
