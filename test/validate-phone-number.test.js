import { validatePhoneNumber } from '../src';

describe('validatePhoneNumber', () => {
    it('Accepts valid numbers', () => {
        expect(validatePhoneNumber('0400000000')).toEqual(true);
    });

    it('Rejects the empty string', () => {
        expect(validatePhoneNumber('')).toEqual(false);
    });

    it('Rejects numbers that are too short', () => {
        expect(validatePhoneNumber('00000')).toEqual(false);
    });

    it('Rejects numbers that are too long', () => {
        expect(validatePhoneNumber('0000000000000000000000000')).toEqual(false);
    });

    it('Rejects invalid characters', () => {
        expect(validatePhoneNumber('000000000f')).toEqual(false);
    });
});
