import { getSwatchCss } from '../src';

describe('getSwatchCss', () => {
    it('Shows background images', () => {
        expect(getSwatchCss('/path/to/background/image.png')).toEqual({
            background:
                'url(/media/attribute/swatch/path/to/background/image.png) no-repeat center'
        });
    });

    it('Shows Hex colour bgd', () => {
        expect(getSwatchCss('#00CCFF')).toEqual({
            background: '#00CCFF'
        });
    });

    it('Shows background gradient', () => {
        expect(getSwatchCss('#00CCFF,#009999')).toEqual({
            background: 'linear-gradient(to right, #00CCFF 0%, #00CCFF 50%, #009999 50%, #009999 100%)'
        });
    });

    it('Shows no background', () => {
        expect(getSwatchCss('')).toEqual({
            background: ''
        });
    });
});
