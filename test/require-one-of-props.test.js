import { requiredOneOfProps } from '../src';

describe('requiredOneOfProps util function', () => {
    it('accepts a prop when the conditions are satisfied', () => {
        const props = {
            name: 'Test name',
            text: 'Test text'
        };
        const propName = 'text';
        const componentName = 'TestComponent';
        const result = requiredOneOfProps(['text', 'children'])(props, propName, componentName);
        expect(result).toBe(undefined);
    });

    it('returns an error when the conditions are not satisfied', () => {
        const props = {
            name: 'Test name'
        };
        const propName = 'text';
        const componentName = 'TestComponent';
        const result = requiredOneOfProps(['text', 'children'])(props, propName, componentName);
        const resultIsError = result instanceof Error;
        expect(resultIsError).toBe(true);
    });
});
