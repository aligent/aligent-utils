import { urlEncodedObjectToFormData, objectToFormData } from '../src';

describe('objectToFormData', () => {
    it('Converts a one level "deep" object', () => {
        const data = {
            name: 'Bob',
            address: '123 Cool Street'
        };

        expect(objectToFormData(data)).toEqual('name=Bob&address=123 Cool Street');
    });

    it('Converts a nested object', () => {
        const data = {
            name: 'Bob',
            address: {
                street: '123 Cool Street',
                suburb: 'Adelaide'
            }
        };

        expect(objectToFormData(data)).toEqual('name=Bob&address[street]=123 Cool Street&address[suburb]=Adelaide');
    });

    it('Converts and object with an array', () => {
        const data = {
            name: 'Bob',
            children: [
                'Amy',
                'Tom'
            ]
        };

        expect(objectToFormData(data)).toEqual('name=Bob&children[0]=Amy&children[1]=Tom');
    });
});

describe('urlEncodedObjectToFormData', () => {
    it('urlEncodes a data object', () => {
        const data = {
            fruit: ['apple', 'banana']
        };

        expect(urlEncodedObjectToFormData(data)).toEqual('fruit%5B0%5D=apple&fruit%5B1%5D=banana');
    });
});
