import { getRegions } from '../src';

describe('get_regions', () => {
    const testRegions = [
        { country_id: 'AU', name: 'South Australia' },
        { country_id: 'NZ', name: 'Not a real region' }
    ];

    it('Returns the empty array for New Zealand', () => {
        expect(getRegions('NZ', testRegions)).toEqual([]);
    });

    it('Filters returns the filtered regions', () => {
        expect(getRegions('AU', testRegions)).toEqual([
            { country_id: 'AU', name: 'South Australia' }
        ]);
    });
});
