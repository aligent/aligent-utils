import { isValidEmail } from '../src';

describe('isValidEmail', () => {
    it('Validates a reasonable email', () => {
        expect(isValidEmail('nice@realemail.co')).toEqual(true);
    });

    it('Rejects an unreasonable email', () => {
        expect(isValidEmail('notarealemail')).toEqual(false);
    });
});
