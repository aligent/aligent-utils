import { getProductOption } from '../src';

describe('getProductOption', () => {

    const option = {
        is_in_stock: true,
        option_id: 2,
        option_label: 'color',
        option_value: '54',
        product_id: '17014',
        selected: true,
        sku: '634',
        value: 'red',
    };

    const testProduct = {
        configurable_attributes: [
            {
                attribute_code: 'color',
                attribute_id: 2,
                options: [option],
            },
        ],
    };

    it('Gets product option', () => {
        expect(getProductOption(testProduct, 'color')).toEqual(option);
    });

    it('Gets simple product option', () => {
        expect(getProductOption({ configurable_attributes: [] }, 'color')).toEqual(false);
    });

    it('Doesn\'t get missing product options', () => {
        expect(getProductOption({}, 'size')).toEqual(false);
    });
});
