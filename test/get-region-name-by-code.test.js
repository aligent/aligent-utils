import { getRegionNameByCode } from '../src';

describe('getRegionNameByCode', () => {
    const testRegions = [
        { country_id: 'AU', title: 'South Australia', value: '1' }
    ];

    it('Gets a region name given a code', () => {
        expect(getRegionNameByCode('AU', testRegions, '1')).toEqual('South Australia');
    });

    it('Returns empty string when code is empty', () => {
        expect(getRegionNameByCode('AU', testRegions, '')).toEqual('');
    });

    it('Returns empty string when region not found', () => {
        expect(getRegionNameByCode('AU', testRegions, '2')).toEqual('');
    });
});
