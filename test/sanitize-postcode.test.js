import { sanitizePostcode } from '../src';

describe('sanitizePostcode', () => {
    it('Does nothing to numbers of length shorter than or equal to four', () => {
        expect(sanitizePostcode('000')).toEqual('000');
        expect(sanitizePostcode('0000')).toEqual('0000');
    });

    it('Shortens numbers longer than four', () => {
        expect(sanitizePostcode('00000')).toEqual('0000');
    });

    it('Removes things that aren\'t numbers', () => {
        expect(sanitizePostcode('000f')).toEqual('000');
    });
});
