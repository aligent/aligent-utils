import { splitAndFormatStreet } from '../src';

describe('splitAndFormatStreet', () => {
    it('Wraps string in array', () => {
        expect(splitAndFormatStreet('123 king william st')).toEqual(['123 king william st']);
    });

    it('Single street in array', () => {
        expect(splitAndFormatStreet(['456 king william st'])).toEqual(['456 king william st']);
    });

    it('double street in array', () => {
        expect(splitAndFormatStreet(['Level 1', '789 king william st'])).toEqual([
            'Level 1',
            '789 king william st'
        ]);
    });
});
