import { isDiscountCode } from '../src';

describe('is-discount-code', () => {
    it('correctly identifies a discount code', () => {
        expect(isDiscountCode('Discount (test)')).toEqual(true);
    });

    it('correctly identifies a catalog price rule', () => {
        expect(isDiscountCode('Discount')).toEqual(false);
    });
});
