import { processError } from '../src';

describe('processError', () => {
    it('handles error without parameters', () => {
        const errorObj = {
            message: 'Generic Error Message'
        };
        expect(processError(errorObj)).toEqual(errorObj.message);
    });

    it('handles parsing integer parameters', () => {
        const errorObj = {
            message: 'There needs to be at least %1 characters, instead found %2',
            parameters: [
                8,
                6
            ]
        };

        expect(processError(errorObj)).toEqual("There needs to be at least '8' characters, instead found '6'");
    });

    it('handles parsing string parameters', () => {
        const errorObj = {
            message: '"%fieldName" is required',
            parameters: {
                fieldName: 'email'
            }
        };

        expect(processError(errorObj)).toEqual("'email' is required");
    });
});
