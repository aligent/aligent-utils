import { formatPrice } from '../src';

describe('formatPrice', () => {
    it('formats a price', () => {
        expect(formatPrice(2, '$%s')).toEqual('$2');
    });

    it('formats a price with decimals', () => {
        expect(formatPrice(2, '$%s', true)).toEqual('$2.00');
    });

    it('formats a negative price', () => {
        expect(formatPrice(-2, '$%s')).toEqual('-$2');
    });
});
