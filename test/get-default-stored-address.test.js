import { getDefaultStoredAddress } from '../src';

describe('getDefaultStoredAddress', () => {
    it('Gets a default address', () => {
        const addresses = [{
            id: 1,
            default_shipping: true
        }];
        expect(getDefaultStoredAddress(addresses, 'shipping')).toEqual({
            id: 1,
            default_shipping: true
        });
    });

    it('Returns undefined when there is no default', () => {
        const addresses = [{
            id: 1,
            default_shipping: true
        }];
        expect(getDefaultStoredAddress(addresses, 'billing')).toEqual({
            id: 1,
            default_shipping: true
        });
    });
});
