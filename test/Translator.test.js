import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { TranslatorController } from '../src/index';

const mock = new MockAdapter(axios);


describe('Translator', () => {

    const translations = { 'My Cart': 'Bag' };
    mock.onGet('/translationUrl').reply(200, translations);
    const translator = new TranslatorController();

    // mock __ function due to mock issues
    const __ = (term) => translator.translate(term);

    translator.init('/translationUrl');

    it('Translate to fetch translation', async () => {
        await translator.init('/translationUrl')
        expect(translator.dictionary).toEqual(translations);
    });

    it('Translates a term', () => {
        expect(translator.translate('My Cart')).toEqual('Bag');
    });

    it('Translates a term in shorthand', () => {
        expect(__('My Cart')).toEqual('Bag');
    });

    it("Doesn't translate a term that isn't there", () => {
        expect(translator.translate('Something else')).toEqual('Something else');
    });
});
