# Deployment Instructions

The following steps should be followed to deploy a new version of aligent-utils to npmjs.com

1. Ensure you have write access to the Aligent organisation on NPM
    - If not, then we will have to ask someone who _does_ have access to publish for us

2. Ensure you're sitting at the same point as the current `master` branch.

3. Run the build command, `npm run build`
   
4. Increase version number appropriately, using `npm verion major|minor|patch`, according to semver based on the changes that are being published.
    - Running this command will automatically update `version` in `package.json` and `package-lock.json`, as well as actually committing those changes, and also create a GIT tag

5. Run the publish command, `npm publish --scope=@aligent --public`

6. Push changes to remote
